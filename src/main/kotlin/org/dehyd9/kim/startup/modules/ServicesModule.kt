package org.dehyd9.kim.startup.modules

import dagger.Module
import dagger.Provides
import org.dehyd9.kim.services.*
import org.dehyd9.kim.services.impl.*
import javax.inject.Singleton

@Module
class ServicesModule {

    @Singleton @Provides fun providesOpenFileService(openFileService: FileServiceImpl) :FileService {
        return openFileService
    }

    @Singleton @Provides fun providesSaveFileService(saveFileService: SaveFileServiceImpl) :SaveFileService {
        return saveFileService
    }

    @Singleton @Provides fun providesTextFetchService(textFetchService: TextFetchServiceImpl) :TextFetchService {
        return textFetchService
    }

    @Singleton @Provides fun providesUndoService(undoService: UndoServiceImpl) :UndoService {
        return undoService
    }

    @Singleton @Provides fun providesRawByteFetchService(rawByteFetchService: RawByteFetchServiceImpl): RawByteFetchService {
        return rawByteFetchService
    }
}
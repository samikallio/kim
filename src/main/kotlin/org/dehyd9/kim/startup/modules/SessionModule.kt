package org.dehyd9.kim.startup.modules

import dagger.Module
import dagger.Provides
import org.dehyd9.kim.sessions.SessionManager
import org.dehyd9.kim.sessions.impl.SessionManagerImpl
import javax.inject.Singleton

@Module
class SessionModule {
    @Singleton @Provides fun providesSessionManager(sessionManager: SessionManagerImpl) :SessionManager {
        return sessionManager
    }
}
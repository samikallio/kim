package org.dehyd9.kim.startup.modules

import dagger.Module
import dagger.Provides
import org.dehyd9.kim.commands.registry.interfaces.CommandRegistryService
import org.dehyd9.kim.commands.services.ShortcutService
import org.dehyd9.kim.gui.interfaces.KimGUI
import org.dehyd9.kim.gui.interfaces.impl.KimGUIImpl
import org.dehyd9.kim.parsers.TextCommandParser
import org.dehyd9.kim.persistence.services.InsertionManagerService
import org.dehyd9.kim.services.TextFetchService
import org.dehyd9.kim.services.UndoService
import org.dehyd9.kim.sessions.SessionManager
import javax.inject.Singleton

@Module
class KimGuiModule {
    @Singleton @Provides fun provideKimGUI(kimCommandRegistryService: CommandRegistryService,
                                           textCommandParser: TextCommandParser,
                                           textFetchService: TextFetchService,
                                           insertionManagerService: InsertionManagerService,
                                           sessionManager: SessionManager,
                                           shortcutService: ShortcutService,
                                           undoService: UndoService) :KimGUI {
        return KimGUIImpl(
                kimCommandRegistryService,
                textCommandParser,
                textFetchService,
                insertionManagerService,
                sessionManager,
                shortcutService,
                undoService)
    }
}

package org.dehyd9.kim.startup.modules

import dagger.Module
import dagger.Provides
import org.dehyd9.kim.commands.services.ShortcutService
import org.dehyd9.kim.commands.services.impl.ShortcutServiceImpl

@Module
class ShortcutServiceModule {
    @Provides fun providesShortcutService(shortCutService: ShortcutServiceImpl): ShortcutService {
        return shortCutService
    }
}
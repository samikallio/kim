package org.dehyd9.kim.startup.modules

import dagger.Module
import dagger.Provides
import org.dehyd9.kim.commands.registry.interfaces.CommandRegistryService
import org.dehyd9.kim.commands.services.ShortcutService
import org.dehyd9.kim.parsers.TextCommandParser
import org.dehyd9.kim.parsers.impl.TextCommandParserImpl

@Module
class ParsersModule {
    @Provides fun provideTextCommandParser(shortcutService: ShortcutService, kimCommandRegistryService:
        CommandRegistryService) :TextCommandParser {
        return TextCommandParserImpl(shortcutService, kimCommandRegistryService)
    }
}
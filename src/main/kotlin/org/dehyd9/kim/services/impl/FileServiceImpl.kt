package org.dehyd9.kim.services.impl

import org.dehyd9.kim.services.FileService
import java.io.File
import java.nio.file.FileSystems
import java.nio.file.Files
import javax.inject.Inject


class FileServiceImpl @Inject constructor() :FileService {
    override fun getFileFromPath(path: String): File? {
        val file = File(path)
        if (file.canRead()) {
            return file
        }
        return null
    }

    override fun isFileExists(path: String): Boolean = Files.exists(FileSystems.getDefault().getPath(path))
}
package org.dehyd9.kim.services.impl

import org.apache.tika.parser.txt.CharsetDetector
import org.dehyd9.kim.services.RecognizeCharsetService
import java.io.File
import java.nio.charset.Charset

class RecognizeCharsetServiceImpl : RecognizeCharsetService {

    override fun findCharsetForFile(file: File): Charset? {
        if(!file.exists() || !file.canRead()) {
            return null
        }

        val charsetDetector = CharsetDetector()

        return null
    }
}
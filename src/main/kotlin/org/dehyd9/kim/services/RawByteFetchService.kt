package org.dehyd9.kim.services

import java.io.File

interface RawByteFetchService {
    fun retrieveByteArrayFromPositionToPositionFromFilePath(
            bufferSize: Int, fromPosition: Long, toPosition: Long, fromFile: File): ByteArray
}
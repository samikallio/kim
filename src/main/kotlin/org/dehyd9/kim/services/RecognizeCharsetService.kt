package org.dehyd9.kim.services

import java.io.File
import java.nio.charset.Charset

interface RecognizeCharsetService {
    fun findCharsetForFile(file :File) :Charset?
}
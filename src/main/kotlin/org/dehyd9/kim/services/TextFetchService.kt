package org.dehyd9.kim.services

import kotlinx.coroutines.channels.Channel
import java.io.Reader

interface TextFetchService {

    suspend fun retrieveThisManyCharsFromPositionToPositionFromFilePath(
            howManyChars: Int, fromPosition: Long, toPosition: Long, fromFilePath: String): Channel<Char>

    fun retrieveCharsFromFilePath(
            howManyChars: Int, fromFilePath: String
    ): Channel<Char>

    fun retrieveCharsFromFilePathStartingAtIndex(
            howManyChars: Int, startIndex: Int, fromFilePath: String
    ): Channel<Char>

    fun getReaderFromFileAtPath(fromFilePath: String): Reader?

    fun retrieveThisManyLinesFromFilePathStartingAtIndex(thisManyLines: Int, lineMaxLength: Int,
                                                         startIndex: Int, fromFilePath: String): Channel<String>
}
package org.dehyd9.kim.services

import java.io.File

interface FileService {
    fun isFileExists(path: String): Boolean
    fun getFileFromPath(path: String): File?
}
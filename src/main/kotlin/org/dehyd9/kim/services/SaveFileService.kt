package org.dehyd9.kim.services

import java.io.File
import java.util.*

interface SaveFileService {
    fun saveFile(file: File, fileUUID: UUID) :Boolean
}
package org.dehyd9.kim.services

import org.dehyd9.kim.commands.results.ExecutedCommand

interface UndoService {
    fun pushExecutedCommandToCompletedCommandsStack(executedCommand: ExecutedCommand)
    fun undoLastCommand()
    fun redoLastCommand()
}
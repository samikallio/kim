package org.dehyd9.kim.persistence.services

import com.j256.ormlite.jdbc.JdbcConnectionSource
import org.dehyd9.kim.returnvalues.DatabaseOperationReturnValues

interface DatabaseManager {
    fun getDatabaseConnectionForSession() :JdbcConnectionSource?
    fun initializeDatabaseForSession(databaseFilename: String): DatabaseOperationReturnValues
    fun dropDatabaseForSession(databaseFilename: String): Int
}

package org.dehyd9.kim.persistence.services.impl

import org.dehyd9.kim.commands.enums.ExecutedCommandStatus
import org.dehyd9.kim.commands.parameters.CommandParameter
import org.dehyd9.kim.commands.results.ExecutedCommand
import org.dehyd9.kim.persistence.dao.getInsertionDao
import org.dehyd9.kim.persistence.dao.getKimKeyStrokeDao
import org.dehyd9.kim.persistence.dto.InsertionDTO
import org.dehyd9.kim.persistence.dto.KimKeyStrokeDTO
import org.dehyd9.kim.persistence.entities.Insertion
import org.dehyd9.kim.persistence.services.DatabaseManager
import org.dehyd9.kim.persistence.services.InsertionManagerService
import org.dehyd9.kim.services.UndoService
import org.slf4j.LoggerFactory
import java.util.*
import java.util.stream.Collectors
import javax.inject.Inject

class InsertionManagerServiceImpl @Inject constructor(
        private val databaseManager: DatabaseManager,
        private val undoService: UndoService) : InsertionManagerService {

    private val LOGGER = LoggerFactory.getLogger(InsertionManagerServiceImpl::class.java)

    companion object {
        const val ORDER_ASCENDING = true
        const val ORDER_DESCENDING = false
    }

    override fun persistKeyStroke(kimKeyStrokeDTO: KimKeyStrokeDTO) {
        val kimKeyStroke = kimKeyStrokeDTO.toKimKeyStroke()
        LOGGER.debug("Persisting kimKeyStore -> $kimKeyStrokeDTO")
        val jdbcConnectionSource = databaseManager.getDatabaseConnectionForSession()
        jdbcConnectionSource?.apply {
            val kimKeyStrokeDao = getKimKeyStrokeDao(jdbcConnectionSource)
            kimKeyStrokeDao.create(kimKeyStroke)
        }
    }

    override fun retrieveListOfKimKeyStrokesForUUID(uuid: UUID?): List<KimKeyStrokeDTO>? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun retrieveLastKimKeyStrokeForUUID(uuid: UUID?): KimKeyStrokeDTO? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun persistInsertion(insertionDTO: InsertionDTO) {
        val insertion = insertionDTO.toInsertion()

        val insertionCommandParameter = CommandParameter(parameterName = "insertion", parameterValue= insertion)
        val executedInsertionCommand = ExecutedCommand(commandName = "insertionCommand", commandStatus = ExecutedCommandStatus.OK, commandReturnValue = null,
                commandParameters = setOf(insertionCommandParameter))
        this.undoService.pushExecutedCommandToCompletedCommandsStack(executedInsertionCommand)
        val jdbcConnectionSource = databaseManager.getDatabaseConnectionForSession()

        if (jdbcConnectionSource != null) {
            val insertionDao = getInsertionDao(jdbcConnectionSource)
            insertionDao.create(insertion)
        } else {
            LOGGER.error("Could not get a JDBC connection source, undo functionality disabled")
        }
    }

    override fun retrieveLastInsertionForUUID(uuid: UUID?): InsertionDTO? {
        val jdbcConnectionSource = databaseManager.getDatabaseConnectionForSession()
        if (jdbcConnectionSource != null) {
            val changesDao = getInsertionDao(jdbcConnectionSource)
            val queryBuilder = changesDao.queryBuilder()
            queryBuilder.where().eq(Insertion.UUID_FIELDNAME, uuid)
            queryBuilder.orderBy(Insertion.ID_FIELDNAME, ORDER_DESCENDING)

            val insertion = changesDao.queryForFirst(queryBuilder.prepare())
            insertion?.let { return it.toInsertionDTO() }
            return null
        } else {
            LOGGER.error("Could not get a JDBC connection source, undo functionality disabled")
            return null
        }
    }

    override fun retrieveListOfInsertionsForUUID(uuid: UUID?): List<InsertionDTO>? {
        val jdbcConnectionSource = databaseManager.getDatabaseConnectionForSession()
        if (jdbcConnectionSource != null) {
            val changesDao = getInsertionDao(jdbcConnectionSource)
            val queryBuilder = changesDao.queryBuilder()

            queryBuilder.where().eq(Insertion.UUID_FIELDNAME, uuid)
            queryBuilder.orderBy(Insertion.ID_FIELDNAME, ORDER_ASCENDING)

            val insertions = changesDao.query(queryBuilder.prepare())

            return insertions.stream()
                    .map { insertion -> insertion.toInsertionDTO() }
                    .collect(Collectors.toList())
        } else {
            LOGGER.error("Could not get a JDBC connection source, undo functionality disabled")
            return null
        }
    }
}
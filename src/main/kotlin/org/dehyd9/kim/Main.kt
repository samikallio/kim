package org.dehyd9.kim

import org.dehyd9.kim.startup.components.DaggerKimGuiComponent
import org.dehyd9.kim.startup.modules.KimGuiModule

fun main() {
    val kimGUI = DaggerKimGuiComponent.builder().kimGuiModule(KimGuiModule()).build()
    kimGUI.instance().createAndShowGUI()
}

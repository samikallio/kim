package org.dehyd9.kim.gui.enums

enum class ControlCharacter(val stringValue: String) {
    DELETE_CHARACTER("DELETE_OPERATION"),
    NEW_LINE("NEW_LINE_OPERATION")
}
package org.dehyd9.kim.gui.renderers

import com.googlecode.lanterna.gui2.TextBox

class KimTextBoxRenderer :TextBox.DefaultTextBoxRenderer() {

    init {
        this.setHideScrollBars(true)
    }
}
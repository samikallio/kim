package org.dehyd9.kim.gui.panels

import com.googlecode.lanterna.input.KeyStroke
import com.googlecode.lanterna.input.KeyType
import org.dehyd9.kim.gui.enums.Mode
import org.dehyd9.kim.states.State
import org.dehyd9.kim.states.enums.InputBubbling

class InputState : State {
    override fun handle(keyStroke: KeyStroke, mainPanel: MainPanel): Boolean {
        if(keyStroke.keyType == KeyType.Escape) {
            mainPanel.currentState = KeyCommandState()
            mainPanel.mode = Mode.KEY_COMMAND
            return InputBubbling.INPUT_HANDLED.isInputHandled
        }
        return InputBubbling.INPUT_HANDLED_IN_PARENT.isInputHandled
    }
}
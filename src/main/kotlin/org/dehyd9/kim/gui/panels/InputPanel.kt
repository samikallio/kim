package org.dehyd9.kim.gui.panels

import com.googlecode.lanterna.TerminalSize
import com.googlecode.lanterna.gui2.Panel
import com.googlecode.lanterna.input.KeyStroke
import com.googlecode.lanterna.input.KeyType
import org.dehyd9.kim.commands.services.ShortcutService
import org.dehyd9.kim.controllers.InputTextController
import org.dehyd9.kim.gui.components.InputTextBox
import org.dehyd9.kim.persistence.services.InsertionManagerService
import org.dehyd9.kim.services.TextFetchService
import org.dehyd9.kim.services.UndoService

class InputPanel(screenColumns: Int,
                 insertionManagerService: InsertionManagerService,
                 shortcutService: ShortcutService,
                 undoService: UndoService,
                 textFetchService: TextFetchService,
                 val inputTextController: InputTextController,
                 private val screenRows: Int): Panel() {

    companion object {
        const val NUMBER_OF_ROWS_NEEDED_TO_FIT_TO_WINDOW = 3
    }

    private val inputPanelTextBox = InputTextBox(
            preferredSize = TerminalSize(screenColumns, getPreferredRows()),
            inputTextController =  inputTextController,
            insertionManagerService = insertionManagerService,
            shortcutService = shortcutService,
            undoService = undoService,
            textFetchService = textFetchService)

    private val statusPanel = StatusPanel()

    var containsKimInspiredText = true

    init {
        setInputTextReadOnly()
        setFocusToInputText()
        this.addComponent(inputPanelTextBox)
        this.addComponent(statusPanel)
    }

    fun setFocusToInputText() {
        inputPanelTextBox.takeFocus()
    }

    fun setInputTextWritable() {
        if(containsKimInspiredText) {
            containsKimInspiredText = false
            inputPanelTextBox.text = ""
        }
        inputPanelTextBox.textReadOnly = false
        statusPanel.inputTextBoxWritable()
    }

    fun setInputPanelPreferredSize(terminalSize: TerminalSize) {
        inputPanelTextBox.preferredSize = TerminalSize(terminalSize.columns,
                terminalSize.rows - NUMBER_OF_ROWS_NEEDED_TO_FIT_TO_WINDOW)
        inputPanelTextBox.invalidate()
    }

    fun setTextForInputPanel(text :String) {
        this.inputPanelTextBox.text = text
    }

    override fun handleInput(key: KeyStroke?): Boolean {

        if(key?.keyType == KeyType.Escape) {
            whenEscIsPressedExitTheTextEditingState()
            return false
        }

        return !inputPanelTextBox.textReadOnly

    }

    private fun whenEscIsPressedExitTheTextEditingState() {
      setInputTextReadOnly()
    }

    private fun getPreferredRows(): Int {
        return screenRows - NUMBER_OF_ROWS_NEEDED_TO_FIT_TO_WINDOW
    }

    private fun setInputTextReadOnly() {
        inputPanelTextBox.textReadOnly = true
        statusPanel.inputTextBoxReadOnly()
    }
}
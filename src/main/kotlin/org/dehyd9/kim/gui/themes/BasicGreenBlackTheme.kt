package org.dehyd9.kim.gui.themes

import com.googlecode.lanterna.TextColor
import com.googlecode.lanterna.graphics.SimpleTheme
import com.googlecode.lanterna.graphics.Theme

class BasicGreenBlackTheme {

    companion object {

        fun getBasicGreenBlackTheme():Theme {
            return SimpleTheme(TextColor.ANSI.GREEN, TextColor.ANSI.BLACK)
        }
    }
}
package org.dehyd9.kim.gui.interfaces.impl

import com.googlecode.lanterna.TerminalSize
import com.googlecode.lanterna.TextColor
import com.googlecode.lanterna.gui2.DefaultWindowManager
import com.googlecode.lanterna.gui2.EmptySpace
import com.googlecode.lanterna.gui2.MultiWindowTextGUI
import com.googlecode.lanterna.gui2.Window
import com.googlecode.lanterna.screen.Screen
import com.googlecode.lanterna.screen.TerminalScreen
import com.googlecode.lanterna.terminal.DefaultTerminalFactory
import com.googlecode.lanterna.terminal.SimpleTerminalResizeListener
import com.googlecode.lanterna.terminal.Terminal
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import org.dehyd9.kim.commands.registry.interfaces.CommandRegistryService
import org.dehyd9.kim.commands.services.ShortcutService
import org.dehyd9.kim.gui.interfaces.KimGUI
import org.dehyd9.kim.gui.themes.BasicGreenBlackTheme.Companion.getBasicGreenBlackTheme
import org.dehyd9.kim.gui.windows.KimMainWindow
import org.dehyd9.kim.parsers.TextCommandParser
import org.dehyd9.kim.persistence.services.InsertionManagerService
import org.dehyd9.kim.services.TextFetchService
import org.dehyd9.kim.services.UndoService
import org.dehyd9.kim.sessions.SessionManager
import java.util.*
import javax.inject.Inject
import kotlin.properties.Delegates

class KimGUIImpl @Inject constructor(
        kimCommandRegistryService: CommandRegistryService,
        textCommandParser: TextCommandParser,
        textFetchService: TextFetchService,
        insertionManagerService: InsertionManagerService,
        sessionManager: SessionManager,
        val shortcutService: ShortcutService,
        val undoService: UndoService
                ):KimGUI {

    private val kimWindow: KimMainWindow
    private val kimScreen: Screen
    private val terminal: Terminal

    init {
        terminal = createDefaultTerminal()

        if(terminal is SwingTerminalFrame) {
            terminal.addResizeListener(KimTerminalListener(terminal.terminalSize))
            terminal.extendedState = SwingTerminalFrame.MAXIMIZED_BOTH
        }

        kimScreen = TerminalScreen(terminal)
        kimScreen.refresh(Screen.RefreshType.AUTOMATIC)

        kimWindow = KimMainWindow(terminalSize =  kimScreen.terminalSize,
                kimCommandRegistryService =  kimCommandRegistryService,
                textCommandParser = textCommandParser,
                textFetchService = textFetchService,
                insertionManagerService = insertionManagerService,
                sessionManager = sessionManager,
                shortcutService = shortcutService,
                undoService = undoService)
    }

    override fun createAndShowGUI() {

        kimScreen.startScreen()

        kimWindow.theme = getBasicGreenBlackTheme()
        kimWindow.setHints(listOf(Window.Hint.FIT_TERMINAL_WINDOW))

        val gui = MultiWindowTextGUI(kimScreen, DefaultWindowManager(), EmptySpace(TextColor.ANSI.BLACK))
        gui.addWindowAndWait(kimWindow)
    }

    private fun createDefaultTerminal(): Terminal {
        return DefaultTerminalFactory().setTerminalEmulatorTitle("Kim").createTerminal()
    }

    @ExperimentalCoroutinesApi
    @ObsoleteCoroutinesApi
    inner class KimTerminalListener(terminalSize: TerminalSize) :SimpleTerminalResizeListener(terminalSize) {
        override fun onResized(terminal: Terminal?, newSize: TerminalSize?) {
            kimWindow?.updateMainPanelOnTerminalResized(newSize)
            super.onResized(terminal, newSize)
        }
    }
}
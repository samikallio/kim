package org.dehyd9.kim.gui.windows

import com.googlecode.lanterna.TerminalSize
import com.googlecode.lanterna.gui2.BasicWindow
import com.googlecode.lanterna.gui2.Panel
import com.googlecode.lanterna.gui2.TextBox
import com.googlecode.lanterna.gui2.Window
import com.googlecode.lanterna.input.KeyStroke
import com.googlecode.lanterna.input.KeyType
import org.dehyd9.kim.commands.enums.ExecutedCommandStatus
import org.dehyd9.kim.commands.interfaces.CommandResultListener
import org.dehyd9.kim.commands.interfaces.impl.NotFoundExecutableCommandImpl
import org.dehyd9.kim.commands.interfaces.impl.OpenFileExecutableCommandImpl
import org.dehyd9.kim.commands.interfaces.impl.QuitExecutableCommandImpl
import org.dehyd9.kim.commands.interfaces.impl.SaveFileExecutableCommandImpl
import org.dehyd9.kim.commands.registry.interfaces.CommandRegistryService
import org.dehyd9.kim.commands.results.ExecutedCommand
import org.dehyd9.kim.controllers.InputTextController
import org.dehyd9.kim.gui.themes.BasicGreenBlackTheme.Companion.getBasicGreenBlackTheme
import org.dehyd9.kim.parsers.TextCommandParser
import java.util.*
import kotlin.system.exitProcess

class TextCommandWindow(private val commandResultListener: CommandResultListener,
                        private val inputTextController: InputTextController? = null,
                        val kimCommandRegistryService: CommandRegistryService,
                        private val textCommandParser: TextCommandParser) : BasicWindow(), CommandResultListener {

    companion object {
        const val TEXT_COMMAND_WINDOW_WIDTH = 60
        const val TEXT_COMMAND_WINDOW_HEIGHT = 1
    }


    private val commandPanel: Panel = Panel()
    private val commandTextBox = TextBox(TerminalSize(TEXT_COMMAND_WINDOW_WIDTH, TEXT_COMMAND_WINDOW_HEIGHT))

    init {
        this.setHints(listOf(Window.Hint.CENTERED, Window.Hint.MODAL))
        this.setCloseWindowWithEscape(true)
        commandPanel.addComponent(commandTextBox)
        this.component = commandPanel
        this.theme = getBasicGreenBlackTheme()
        this.title = "Enter your command - type ? for help"
    }

    override fun handleInput(key: KeyStroke?): Boolean {

        if(key?.keyType == KeyType.Enter) {
            textCommandParser.parseCommandAndExecute(commandString = commandTextBox.text,
                    commandResultListener = this,
                    inputTextController = inputTextController)
        }

        return super.handleInput(key)
    }

    override fun reactOnExecutedCommand(executedCommand: ExecutedCommand) {
        when (executedCommand.commandStatus) {
            ExecutedCommandStatus.OK -> {
                when(executedCommand.commandName) {
                    QuitExecutableCommandImpl.getCommandName() -> {
                        commandResultListener.reactOnExecutedCommand(executedCommand)
                    }

                    OpenFileExecutableCommandImpl.getCommandName() -> {
                        commandResultListener.reactOnExecutedCommand(executedCommand)
                        this.close()
                    }

                    NotFoundExecutableCommandImpl.getCommandName() -> {
                        // This text should be in status bar
                        this.commandTextBox.text = "Command not found"
                    }

                    SaveFileExecutableCommandImpl.getCommandName() -> {
                        this.close()
                    }
                }
            }
            ExecutedCommandStatus.WRONG_PARAMETERS -> {
                this.commandTextBox.text = "Wrong parameters"
            }
            ExecutedCommandStatus.ERROR -> {
                when (executedCommand.commandName) {
                    OpenFileExecutableCommandImpl.getCommandName() -> {
                        this.commandTextBox.text = "File not found"
                    }
                    SaveFileExecutableCommandImpl.getCommandName() -> {
                        this.commandTextBox.text = "Some error happened :("
                    }
                }
            }
        }
    }
}
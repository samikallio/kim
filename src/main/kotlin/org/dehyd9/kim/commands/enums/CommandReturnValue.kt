package org.dehyd9.kim.commands.enums

class CommandReturnValue<T> constructor(val status: Status, val value: T) {

    enum class Status {
        OK, ERROR, WRONG_PARAMETERS
    }
}
package org.dehyd9.kim.commands.enums

enum class SystemCommand(val systemCommandName: String) {
    QUIT(systemCommandName = "quit-command"),
    OPEN_FILE(systemCommandName = "open-file"),
    SAVE_FILE(systemCommandName = "save-file"),
    UNDO_COMMAND(systemCommandName = "undo-command"),
    INSERT_MODE(systemCommandName = "insert-mode"),
    COMMAND_NOT_FOUND(systemCommandName =  "command-not-found")
}
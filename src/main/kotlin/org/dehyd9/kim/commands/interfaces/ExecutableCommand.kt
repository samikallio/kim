package org.dehyd9.kim.commands.interfaces

import org.dehyd9.kim.commands.parameters.CommandParameter
import org.dehyd9.kim.commands.results.ExecutedCommand

interface ExecutableCommand {
    fun getCommandName() :String
    fun executeWithParameters(commandParameters: Set<CommandParameter>) : ExecutedCommand
}
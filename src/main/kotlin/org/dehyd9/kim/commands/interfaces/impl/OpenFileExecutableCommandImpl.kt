package org.dehyd9.kim.commands.interfaces.impl

import org.dehyd9.kim.commands.enums.ExecutedCommandStatus
import org.dehyd9.kim.commands.enums.SystemCommand
import org.dehyd9.kim.commands.interfaces.ExecutableCommand
import org.dehyd9.kim.commands.parameters.CommandParameter
import org.dehyd9.kim.commands.results.ExecutedCommand
import org.dehyd9.kim.services.FileService
import org.slf4j.LoggerFactory
import javax.inject.Inject

class OpenFileExecutableCommandImpl @Inject constructor(private val fileService: FileService)
    :ExecutableCommand {

    private val LOGGER = LoggerFactory.getLogger(OpenFileExecutableCommandImpl::class.java)

    companion object {

        fun getCommandName(): String {
            return SystemCommand.OPEN_FILE.systemCommandName
        }
    }

    override fun executeWithParameters(commandParameters :Set<CommandParameter>): ExecutedCommand {

        if (LOGGER.isDebugEnabled) {
            for (commandParameter in commandParameters) {
                LOGGER.debug("parameter -> ${commandParameter.parameterValue?.getParameterValueAsString()}")
            }
        }

        if(commandParameters.isEmpty()) {
            return ExecutedCommand(commandName =  getCommandName(),
                    commandStatus = ExecutedCommandStatus.WRONG_PARAMETERS)
        }

        if(commandParameters.elementAt(1).parameterValue == null) {
            return ExecutedCommand(commandName =  getCommandName(),
                    commandStatus = ExecutedCommandStatus.WRONG_PARAMETERS)
        }

        return if(fileService.isFileExists(commandParameters.elementAt(1).parameterValue!!.getParameterValueAsString()))
                    ExecutedCommand(commandName = getCommandName(), commandStatus = ExecutedCommandStatus.OK,
                            commandReturnValue = commandParameters.elementAt(1).parameterValue!!.getParameterValueAsString())
                else
                    ExecutedCommand(commandName = getCommandName(), commandStatus = ExecutedCommandStatus.ERROR)

    }

    override fun getCommandName(): String {
        return OpenFileExecutableCommandImpl.getCommandName()
    }
}
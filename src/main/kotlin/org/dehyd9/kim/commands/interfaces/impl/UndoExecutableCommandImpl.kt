package org.dehyd9.kim.commands.interfaces.impl

import org.dehyd9.kim.commands.enums.ExecutedCommandStatus
import org.dehyd9.kim.commands.enums.SystemCommand
import org.dehyd9.kim.commands.interfaces.ExecutableCommand
import org.dehyd9.kim.commands.parameters.CommandParameter
import org.dehyd9.kim.commands.results.ExecutedCommand
import org.dehyd9.kim.services.UndoService
import javax.inject.Inject

class UndoExecutableCommandImpl @Inject constructor(private val undoService: UndoService) : ExecutableCommand {

    companion object {
        fun getCommandName(): String {
            return SystemCommand.UNDO_COMMAND.systemCommandName
        }
    }

    override fun getCommandName(): String {
        return UndoExecutableCommandImpl.getCommandName()
    }

    override fun executeWithParameters(commandParameters: Set<CommandParameter>): ExecutedCommand {
        this.undoService.undoLastCommand()
        return ExecutedCommand(getCommandName(), commandStatus = ExecutedCommandStatus.OK)
    }
}
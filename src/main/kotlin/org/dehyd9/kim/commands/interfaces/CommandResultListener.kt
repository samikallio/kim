package org.dehyd9.kim.commands.interfaces

import org.dehyd9.kim.commands.results.ExecutedCommand

interface CommandResultListener {
    fun reactOnExecutedCommand(executedCommand: ExecutedCommand)
}
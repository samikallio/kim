package org.dehyd9.kim.commands.interfaces.impl

import org.dehyd9.kim.commands.enums.ExecutedCommandStatus
import org.dehyd9.kim.commands.enums.SystemCommand
import org.dehyd9.kim.commands.interfaces.ExecutableCommand
import org.dehyd9.kim.commands.parameters.CommandParameter
import org.dehyd9.kim.commands.results.ExecutedCommand

class NotFoundExecutableCommandImpl : ExecutableCommand {


    companion object {

        fun getCommandName(): String {
            return SystemCommand.COMMAND_NOT_FOUND.systemCommandName
        }
    }

    override fun getCommandName(): String {
        return NotFoundExecutableCommandImpl.getCommandName()
    }

    override fun executeWithParameters(commandParameters: Set<CommandParameter>): ExecutedCommand {
        return ExecutedCommand(commandName =  getCommandName(),
                commandStatus = ExecutedCommandStatus.OK)
    }

}
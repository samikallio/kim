package org.dehyd9.kim.commands.interfaces.impl

import org.dehyd9.kim.commands.enums.ExecutedCommandStatus
import org.dehyd9.kim.commands.enums.SystemCommand
import org.dehyd9.kim.commands.interfaces.ExecutableCommand
import org.dehyd9.kim.commands.parameters.CommandParameter
import org.dehyd9.kim.commands.results.ExecutedCommand
import org.dehyd9.kim.services.SaveFileService
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*
import javax.inject.Inject

class SaveFileExecutableCommandImpl @Inject constructor(
        private val saveFileService: SaveFileService): ExecutableCommand {

    private val LOGGER = LoggerFactory.getLogger(SaveFileExecutableCommandImpl::class.java)

    companion object {
        const val COMMAND_HAS_MAX_THREE_PARAMETERS = 3
        const val FILE_ON_DISK_FILENAME_INDEX = 1
        const val FILE_UUID_INDEX = 0
        const val SIZE_IF_FILENAME_IS_GIVEN = 2

        fun getCommandName() = SystemCommand.SAVE_FILE.systemCommandName
    }

    override fun getCommandName() = SaveFileExecutableCommandImpl.getCommandName()

    override fun executeWithParameters(commandParameters: Set<CommandParameter>): ExecutedCommand {

        if(commandParameters.size > COMMAND_HAS_MAX_THREE_PARAMETERS) {
            return ExecutedCommand(getCommandName(), ExecutedCommandStatus.WRONG_PARAMETERS)
        }

        when(commandParameters.size) {
            SIZE_IF_FILENAME_IS_GIVEN -> {
                val uuidParameter = commandParameters.elementAt(FILE_UUID_INDEX)
                val fileOnDiskNameParameter = commandParameters.elementAt(FILE_ON_DISK_FILENAME_INDEX)
                fileOnDiskNameParameter.parameterValue?.let {
                    val fileOnDisk = File(it.getParameterValueAsString())
                    val uuidAsString = uuidParameter.parameterValue?.getParameterValueAsString()

                    if(!fileOnDisk.exists()) {
                        fileOnDisk.createNewFile()
                    }

                    if(saveFileService.saveFile(fileOnDisk, UUID.fromString(uuidAsString))) {
                        return ExecutedCommand(getCommandName(), ExecutedCommandStatus.OK)
                    }
                }

            }
            else -> {
            }
        }

        return ExecutedCommand(getCommandName(), ExecutedCommandStatus.ERROR)

    }
}
package org.dehyd9.kim.commands.services

interface ShortcutService {
    fun getCommandNameFromShortCutKey(shortCutKey: String): String?
}
package org.dehyd9.kim.commands.parameters

class CommandParameterValueImpl(private val parameterValueAsString: String) : CommandParameterValue {
    override fun getParameterValueAsString(): String {
        return parameterValueAsString
    }
}
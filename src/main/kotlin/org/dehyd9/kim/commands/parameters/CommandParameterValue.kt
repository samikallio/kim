package org.dehyd9.kim.commands.parameters

interface CommandParameterValue {
    fun getParameterValueAsString(): String
}
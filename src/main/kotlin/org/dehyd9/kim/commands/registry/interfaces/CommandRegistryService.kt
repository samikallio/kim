package org.dehyd9.kim.commands.registry.interfaces

import org.dehyd9.kim.commands.interfaces.ExecutableCommand

interface CommandRegistryService {
    fun getKimCommandMap(): Map<String, ExecutableCommand>
    fun getKimCommandForName(commandName: String): ExecutableCommand?
}
package org.dehyd9.kim.sessions

import java.util.*

interface SessionManager {
    fun getXorEncryptionKeyForSession(): String
    fun getUUIDForUnsavedFile(): UUID
    fun getFileNameForUUID(uuid: UUID): String?
    fun createUUIDForFileName(fileName: String): UUID
    fun cleanUpWhenShuttingDown()
}
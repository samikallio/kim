package org.dehyd9.kim.sessions

import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.lang3.StringUtils
import java.util.*
import kotlin.collections.HashMap

object Session {
    private const val XOR_ENCRYPTION_KEY_LENGTH = 32
    val UUIDToFilenameMap: MutableMap<UUID, String> = HashMap()
    val xorEncryptionKeyForSession: String = RandomStringUtils.randomAlphanumeric(XOR_ENCRYPTION_KEY_LENGTH)
}
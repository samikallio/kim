package org.dehyd9.kim.parsers.impl

import org.apache.commons.lang3.StringUtils
import org.dehyd9.kim.commands.interfaces.CommandResultListener
import org.dehyd9.kim.commands.interfaces.ExecutableCommand
import org.dehyd9.kim.commands.interfaces.impl.NotFoundExecutableCommandImpl
import org.dehyd9.kim.commands.interfaces.impl.OpenFileExecutableCommandImpl
import org.dehyd9.kim.commands.interfaces.impl.SaveFileExecutableCommandImpl
import org.dehyd9.kim.commands.parameters.CommandParameter
import org.dehyd9.kim.commands.parameters.CommandParameterValueImpl
import org.dehyd9.kim.commands.registry.interfaces.CommandRegistryService
import org.dehyd9.kim.commands.services.ShortcutService
import org.dehyd9.kim.controllers.InputTextController
import org.dehyd9.kim.parsers.TextCommandParser
import org.slf4j.LoggerFactory
import javax.inject.Inject

class TextCommandParserImpl @Inject constructor(private val shortCutService: ShortcutService,
                                                private val commandRegistryService: CommandRegistryService) : TextCommandParser {
    private val LOGGER = LoggerFactory.getLogger(TextCommandParserImpl::class.java)

    companion object {
        private const val SPLIT_ON_SPACE = " "
    }

    override fun parseCommandAndExecute(commandString: String, commandResultListener: CommandResultListener,
                                        inputTextController: InputTextController?) {

        if(StringUtils.isBlank(commandString)) {
            return
        }

        val commandParameters :MutableSet<CommandParameter> = mutableSetOf()
        val splittedCommandString = commandString.split(SPLIT_ON_SPACE)
        val commandFound = getCommand(splittedCommandString[0])

        if(commandFound == null) {
            executeCommand(commandResultListener, NotFoundExecutableCommandImpl(), emptySet())
            return
        }

        when (commandFound.getCommandName()) {
            SaveFileExecutableCommandImpl.getCommandName() -> {
                if(inputTextController?.controllerForUUID != null) {
                    for (s in splittedCommandString) {
                        LOGGER.debug("s -> $s")
                    }
                    val uuidParameter = CommandParameter(
                            parameterName = "fileUUID",
                            parameterValue = CommandParameterValueImpl(inputTextController.controllerForUUID.toString())
                    )
                    val fileNameParameter = CommandParameter(
                            parameterName = "filename",
                            parameterValue = CommandParameterValueImpl(splittedCommandString[1])
                    )
                    commandParameters.add(uuidParameter)
                    commandParameters.add(fileNameParameter)
                }
            }
            OpenFileExecutableCommandImpl.getCommandName() -> {
                val commandNameParameter = CommandParameter(parameterName = "commandName",
                        parameterValue = CommandParameterValueImpl(OpenFileExecutableCommandImpl.getCommandName()))
                val fileNameParameter = CommandParameter(parameterName = "fileName",
                        parameterValue = CommandParameterValueImpl(splittedCommandString[1]))
                commandParameters.apply {
                    this.add(commandNameParameter)
                    this.add(fileNameParameter)
                }
            }
        }

        executeCommand(commandResultListener, commandFound, commandParameters)
    }

    private fun getCommand(commandCandidate :String) :ExecutableCommand? {
        val commandName = shortCutService.getCommandNameFromShortCutKey(commandCandidate)
        commandName?.let {
            return commandRegistryService.getKimCommandForName(it)
        }

        return null
    }

    private fun executeCommand(commandResultListener: CommandResultListener, executableCommand :ExecutableCommand, commandParameters :Set<CommandParameter>) {
        commandResultListener.reactOnExecutedCommand(executableCommand.executeWithParameters(commandParameters))
    }
}
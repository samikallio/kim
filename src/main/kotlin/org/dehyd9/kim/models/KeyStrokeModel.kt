package org.dehyd9.kim.models

import com.googlecode.lanterna.input.KeyType
import java.io.Serializable

data class KeyStrokeModel(
        val keyType: KeyType,
        val character: Char,
        val ctrlDown: Boolean,
        val altDown: Boolean,
        val shiftDown: Boolean,
        val eventTime: Long
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}
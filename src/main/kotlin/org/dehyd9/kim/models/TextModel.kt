package org.dehyd9.kim.models

import java.io.File
import java.nio.charset.Charset

class TextModel(val fileOnDisk: File? = null, private val visibleRows: Int? = DEFAULT_VISIBLE_ROWS,
                        private val charset: Charset? = JVM_DEFAULT_CHARSET) {

    private var visibleText :MutableList<String> = mutableListOf()


    companion object {
        private const val DEFAULT_VISIBLE_ROWS = 40
        private val JVM_DEFAULT_CHARSET = Charset.defaultCharset()
    }

    fun updateVisibleTextWithRows(updatedVisibleRows :Int) {

    }

    fun updateModel() {

    }

    fun getVisibleText() :List<String> {
        return visibleText.toList()
    }

    private fun getCharset() :Charset {
        if(charset == null) {
            return JVM_DEFAULT_CHARSET
        } else {
            //TODO: Use Tika to guesstimate charset of file
            return JVM_DEFAULT_CHARSET
        }
    }

    private fun getVisibleRows() :Int {
        return if(visibleRows == null) {
            DEFAULT_VISIBLE_ROWS
        } else {
            visibleRows
        }
    }
}
package org.dehyd9.kim.controllers

import org.dehyd9.kim.models.TextModel
import org.dehyd9.kim.persistence.entities.Insertion
import org.dehyd9.kim.persistence.services.InsertionManagerService
import org.dehyd9.kim.persistence.services.DatabaseManager
import java.io.File
import javax.inject.Inject

class TextController @Inject constructor(
        val insertionManagerService: InsertionManagerService, val databaseManager: DatabaseManager
) {

    lateinit var textModel :TextModel

    init {

    }

    fun updateTextModel(insertion: Insertion) :TextModel {



        return textModel
    }

    fun createNewText(visibleRows :Int?) :TextModel {
        textModel = TextModel(visibleRows = visibleRows)
        return textModel
    }

    fun createTextFromFile(textFile: File) : TextModel {
        textModel = TextModel(fileOnDisk = textFile)
        return textModel
    }

    fun createTextFromFile(textFile: File, visibleRows: Int) :TextModel {
        textModel = TextModel(fileOnDisk = textFile, visibleRows = visibleRows)
        return textModel
    }
}
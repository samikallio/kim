package org.dehyd9.kim.controllers

import com.googlecode.lanterna.input.KeyStroke
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.receiveOrNull
import kotlinx.coroutines.channels.ticker
import org.dehyd9.kim.gui.components.InputTextBox
import org.dehyd9.kim.models.TextBuffer
import org.dehyd9.kim.persistence.dto.KimKeyStrokeDTO
import org.dehyd9.kim.persistence.services.InsertionManagerService
import org.dehyd9.kim.services.TextFetchService
import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.ConcurrentLinkedDeque
import kotlin.coroutines.CoroutineContext

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
class InputTextController(val controllerForUUID: UUID,
                          val textFetchService: TextFetchService,
                          val filePath: String?,
                          private val insertionManagerService: InsertionManagerService,
                          private val bufferSize: Int = DEFAULT_BUFFER_64K): CoroutineScope {
    private val logger = LoggerFactory.getLogger(InputTextController::class.java)

    private val upperTextBuffer = TextBuffer(DEFAULT_BUFFER_64K)
    private val lowerTextBuffer = TextBuffer(DEFAULT_BUFFER_64K)
    private val viewTextBuffer = TextBuffer(DEFAULT_BUFFER_64K)

    private val viewBufferChannel = Channel<KeyStroke>(bufferSize)

    private val job = Job()

    lateinit var controllerForInputTextBox: InputTextBox

    private var currentPositionOfLowerBuffer = bufferSize.toLong()

    companion object {
        private const val DEFAULT_BUFFER_64K = 64_000
    }

    fun disposeInputController() {
        viewBufferChannel.close()
        job.cancel()
    }

    override val coroutineContext: CoroutineContext
        get() = job

    init {
        filePath?.let {
            fillLowerBufferAtInitFromFilePath(it)
            fillViewBufferAtInit(it)
        }

        receiveFromViewBufferChannel()
    }

    fun getViewBufferChannel() = viewBufferChannel

    fun addStringToUpperBuffer(strings: List<String>) {
        for(string in strings) {
            upperTextBuffer.appendStringToListOfStrings(string)
        }
    }

    fun getTextFromViewBuffer(wrapWidth: Int): List<String> =
            getWrappedTextFromBufferAsListOfLines(viewTextBuffer, wrapWidth)

    fun fillViewTextBufferFromLowerBuffer() {
        viewTextBuffer.fillBufferFromListOfStrings(lowerTextBuffer.getImmutableLineBuffer())
    }

    private fun CoroutineScope.receiveFromViewBufferChannel() {
        launch {
            while (!viewBufferChannel.isClosedForReceive) {
                val receivedKeyStroke = viewBufferChannel.receiveOrNull()
                receivedKeyStroke?.apply {
                    logger.debug("Received -> ${this.character}")
                    insertionManagerService.persistKeyStroke(KimKeyStrokeDTO(
                            forUuid = controllerForUUID,
                            keyType = this.keyType,
                            character = this.character,
                            ctrlDown = this.isCtrlDown,
                            altDown = this.isAltDown,
                            shiftDown = this.isShiftDown,
                            eventTime = this.eventTime
                    ))
                }
            }
        }
    }

    private fun getWrappedTextFromBufferAsListOfLines(textBuffer: TextBuffer, wrapWidth: Int): List<String> {
        val rawLines = textBuffer.getImmutableLineBuffer()
        val wrappedLines = rawLines.toMutableList()
        var columnCounter = 0

        var textBuilder = StringBuilder(wrapWidth)
        for (string in rawLines) {
            textBuilder.append(string)
            if(textBuilder.contains(System.lineSeparator()) || columnCounter >= wrapWidth) {
                val builtString = textBuilder.toString()
                val lineTokenizer = StringTokenizer(builtString, System.lineSeparator())
                while (lineTokenizer.hasMoreTokens()) {
                    wrappedLines.add(lineTokenizer.nextToken())
                }
                columnCounter = 0
                textBuilder = StringBuilder(wrapWidth)
            }
            columnCounter++
        }

        return wrappedLines
    }

    private fun CoroutineScope.fillLowerBufferFromFilePath(filePath: String) {
        launch {
            logger.debug("fillLowerBufferFromFilePath")
            val channel = textFetchService.retrieveThisManyCharsFromPositionToPositionFromFilePath(
                    howManyChars = bufferSize,
                    fromPosition = currentPositionOfLowerBuffer,
                    toPosition = (bufferSize * 2).toLong(),
                    fromFilePath = filePath
            )

            val charList = mutableListOf<Char>()
            for(char in channel) {
                charList.add(char)
            }
            lowerTextBuffer.fillBufferFromListOfChars(charList)
        }
    }

    private fun CoroutineScope.fillViewBufferAtInit(filePath: String) {
        launch {
            logger.debug("Running fillViewBufferAtInit")
            val channel = textFetchService.retrieveThisManyCharsFromPositionToPositionFromFilePath(
                    howManyChars = bufferSize,
                    fromPosition = 0,
                    toPosition = bufferSize.toLong(),
                    fromFilePath = filePath
            )

            val charList = mutableListOf<Char>()
            for(char in channel) {
                charList.add(char)
            }
            viewTextBuffer.fillBufferFromListOfChars(charList)
        }
    }

    private fun CoroutineScope.fillLowerBufferAtInitFromFilePath(filePath: String) {
        launch {
            logger.debug("Running fillLowerBufferAtInitFromFilePath")
            val channel = textFetchService.retrieveThisManyCharsFromPositionToPositionFromFilePath(
                    howManyChars = bufferSize,
                    fromPosition = currentPositionOfLowerBuffer,
                    toPosition = (bufferSize * 2).toLong(),
                    fromFilePath = filePath
            )

            val charList = mutableListOf<Char>()

            for (char in channel) {
                charList.add(char)
            }

            currentPositionOfLowerBuffer = (bufferSize * 2).toLong()

            lowerTextBuffer.fillBufferFromListOfChars(charList)
        }
    }
}
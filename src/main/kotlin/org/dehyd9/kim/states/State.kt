package org.dehyd9.kim.states

import com.googlecode.lanterna.input.KeyStroke
import org.dehyd9.kim.gui.panels.MainPanel

interface State {
    fun handle(keyStroke: KeyStroke, mainPanel: MainPanel): Boolean
}
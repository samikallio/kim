package org.dehyd9.kim.utils

import java.util.*

class KimStringResources {

    companion object {

        private const val KIM_LOGO_KEY = "kim.logo"

        private val resourceBundle = ResourceBundle.getBundle("kim-resources")

        fun getKimLogo() :String {
            return getResourceWithKey(KIM_LOGO_KEY)
        }

        fun getResourceWithKey(key :String):String {
            return resourceBundle.getString(key)
        }
    }
}
package org.dehyd9.kim.utils

import org.apache.commons.lang3.RandomUtils
import org.dehyd9.kim.utils.TempFileNameUtils.AVAILABLE_CHARACTERS
import org.dehyd9.kim.utils.TempFileNameUtils.RANDOM_NAME_LENGTH
import org.dehyd9.kim.utils.TempFileNameUtils.THIS_ALWAYS_PART_OF_NAME

object TempFileNameUtils {
    val AVAILABLE_CHARACTERS = listOf('A'..'Z').flatten()
    const val THIS_ALWAYS_PART_OF_NAME = "kim-"
    const val RANDOM_NAME_LENGTH = 8;
}

fun generateTempFileName(identifier: String? = null) :String {

    val nameBuilder = StringBuilder()
    nameBuilder.append(THIS_ALWAYS_PART_OF_NAME)
    identifier?.let { nameBuilder.append(it) }

    for(i in 0 until RANDOM_NAME_LENGTH) {
        val randomizedIndex = RandomUtils.nextInt(0, AVAILABLE_CHARACTERS.size)
        nameBuilder.append(AVAILABLE_CHARACTERS[randomizedIndex])
    }

    val randomizedNumberToTheFilename = RandomUtils.nextInt(1000,9999)
    nameBuilder.append(randomizedNumberToTheFilename)

    return nameBuilder.toString()
}
